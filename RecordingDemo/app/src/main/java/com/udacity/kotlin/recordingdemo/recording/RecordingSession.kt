package com.udacity.kotlin.recordingdemo.recording

import android.media.CamcorderProfile

data class RecordingSession(val profile: CamcorderProfile, val filename : String) {
    val displayWidth : Int
    val displayHeight : Int
    val frameRate : Int

    init {
        displayWidth = profile.videoFrameWidth
        displayHeight = profile.videoFrameHeight
        frameRate = profile.videoFrameRate
    }
}