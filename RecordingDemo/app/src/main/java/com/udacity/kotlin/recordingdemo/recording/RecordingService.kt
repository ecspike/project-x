package com.udacity.kotlin.recordingdemo.recording

import android.app.Notification
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Binder
import android.os.IBinder

import  android.app.Notification.PRIORITY_MIN


class RecordingService : Service() {
    var isRecording = false
    //lateinit var binder:LocalBinder
    lateinit var recordingInteractor: RecordingInteractorImpl

    init {

    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        recordingInteractor = RecordingInteractorImpl(this.application.applicationContext)

        //binder = LocalBinder()

        if (isRecording)
            return Service.START_NOT_STICKY

        isRecording = true

        var context = getApplicationContext()
        var title = "Now recording"
        var notification = Notification.Builder(context)
                .setContentTitle(title)
                .setAutoCancel(true)
                .setPriority(PRIORITY_MIN)
                .build();

        startForeground(1337, notification);

        val resultCode = intent!!.extras.get("result-code") as Int
        val data = intent!!.extras.get("data") as Intent

        recordingInteractor?.setupRecording(resultCode, data)
        with(recordingInteractor) {
            initRecorder()
            prepareRecorder()
            shareScreen()
        }

        return Service.START_NOT_STICKY
    }

    override fun onDestroy() {
        recordingInteractor.stopScreenSharing()
        recordingInteractor.onDestroy()
        super.onDestroy()
    }



    override fun onBind(intent: Intent?): IBinder? {
        return null
    }
}