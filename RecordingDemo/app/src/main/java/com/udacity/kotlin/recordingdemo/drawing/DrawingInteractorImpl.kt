package com.udacity.kotlin.recordingdemo.drawing

/**
 * Created by james on 2/19/16.
 */
class DrawingInteractorImpl: DrawingInteractor {
    override fun loadImage() {
        throw UnsupportedOperationException()
    }

    override fun saveImage() {
        throw UnsupportedOperationException()
    }
}