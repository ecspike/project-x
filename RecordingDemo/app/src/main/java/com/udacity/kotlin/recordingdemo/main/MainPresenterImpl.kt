package com.udacity.kotlin.recordingdemo.main

import android.Manifest
import android.app.Activity
import android.content.*
import android.content.pm.PackageManager
import android.media.projection.MediaProjectionManager
import android.os.IBinder
import android.provider.Settings
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.widget.Toast
import com.udacity.kotlin.recordingdemo.recording.RecordingService

class MainPresenterImpl(mainView: MainView) : MainPresenter, BroadcastReceiver() {
    val PERMISSION_CODE = 1
    private val PERMISSIONS_STORAGE = arrayOf(
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.RECORD_AUDIO
    )
    var mainView: MainView
    var activity: Activity

    var recordingService: RecordingService? = null

    init {
        this.mainView = mainView
        this.activity = mainView.getActivity()

        verifyDevicePermissions(activity)

        // Setup BroadcastReceiver
        val filter = IntentFilter("com.udacity.kotlin.RecordingMessage")
        activity.registerReceiver(this, filter)

    }

    override fun onDestroy() {
        activity.unregisterReceiver(this)
    }

    fun triggerCaptureIntent() {
        val  manager = activity.getSystemService(Context.MEDIA_PROJECTION_SERVICE) as MediaProjectionManager
        activity.startActivityForResult(manager.createScreenCaptureIntent(), PERMISSION_CODE)
        return
    }

    override fun onRecordingStart() {
        triggerCaptureIntent()

    }

    override fun onRecordingStop() {
        val intent = Intent(activity, RecordingService::class.java)
        activity.applicationContext.stopService(intent)
    }

    override fun onResume() {
        val filter = IntentFilter("com.udacity.kotlin.RecordingMessage")
        activity.registerReceiver(this, filter)
    }

    override fun verifyDevicePermissions(activity: Activity): Int {
        // Check if we have write permission
        val permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED ) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    PERMISSION_CODE
            );
        }



        return permission
    }

    override fun setupRecording(resultCode:Int, data: Intent?) {
        //TODO Pass this to service
        val intent = Intent(activity, RecordingService::class.java)
        // i.putExtra
        intent.putExtra("result-code", resultCode)
        intent.putExtra("data", data)
        activity.applicationContext.startService(intent)
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        val filename = intent?.extras?.get("filename") as String
        mainView.showChangeNameDialog(filename)
    }
}
