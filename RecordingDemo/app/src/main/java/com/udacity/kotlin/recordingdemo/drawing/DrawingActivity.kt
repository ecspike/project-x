package com.udacity.kotlin.recordingdemo.drawing

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Matrix
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.nononsenseapps.filepicker.FilePickerActivity
import com.stephenvinouze.drawingview.DrawingView
import com.udacity.kotlin.recordingdemo.R
import org.jetbrains.anko.toast
import java.util.*


/**
 * Created by james on 2/19/16.
 */

class DrawingActivity: Activity(), NavigationView.OnNavigationItemSelectedListener{
    val TAG = DrawingActivity::class.toString()

    lateinit var drawingView : DrawingView
    lateinit var navView : NavigationView
    lateinit var menu : Menu
    var images : ArrayList<Uri> = arrayListOf<Uri>()
    var imageId = 0

    //lateinit var penThicknessLabel : TextView

    lateinit var drawingToolsPresenter : DrawingToolsPresenterImpl

    val FILE_CODE = 2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_drawing)

        drawingView = findViewById(R.id.drawingView) as DrawingView
        drawingToolsPresenter = DrawingToolsPresenterImpl(this)


        navView = findViewById(R.id.navViewDrawing) as NavigationView
        navView.setItemIconTintList(null)
        menu = (findViewById(R.id.navViewDrawing) as NavigationView).getMenu()
        val item = menu.findItem(R.id.nav_pen_thickness)
        var icon = ColorDrawable(Color.GREEN)
        item.setIcon(icon)

        navView.setNavigationItemSelectedListener(this)

        // Draw the keylines
        //TODO Possibly make this toggleable from the Main Activity
        //val coordinatorLayout = findViewById(R.id.coordinator_drawing) as CoordinatorLayout
        //val designSpec = DesignSpec.fromResource(coordinatorLayout, R.raw.drawing_activity_spec)
        //designSpec.setSpacingsColor(R.color.c_black)
        //drawingView.getOverlay().add(designSpec);

        //hideSystemUI()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
    }

    override fun onResume() {
        super.onResume()
    }

    fun setColorSwatch(id:Int, color:Int) {
        val item = menu.findItem(id)
        var icon = ColorDrawable(color)
        item.setIcon(icon)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == R.id.nav_color_one) {
            // Handle the camera action
            drawingView.setDrawingColor(Color.MAGENTA)
            setColorSwatch(R.id.nav_color_one, Color.MAGENTA)
        } else if (id == R.id.nav_load_image) {
            val intent = Intent(this as Context, FilePickerActivity::class.java)
            intent.putExtra(FilePickerActivity.EXTRA_START_PATH, android.os.Environment.getExternalStoragePublicDirectory(
                    android.os.Environment.DIRECTORY_MOVIES +"/Poe/").getPath())
            intent.putExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, true)
            startActivityForResult(intent, FILE_CODE)
        }
        val drawer = findViewById(R.id.drawer_layout_draw) as DrawerLayout
        drawer.closeDrawer(GravityCompat.START)
        return false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        when (requestCode) {
            FILE_CODE -> {
                if (resultCode != RESULT_OK) {
                    toast("Load Failed")
                    return
                }

                val clip = intent?.clipData

                if (clip != null) {

                    images = ArrayList<Uri>()

                    // Get URIs for the images
                    for (index in 0..clip.itemCount-1) {
                        val uri = clip.getItemAt(index).uri
                        images.add(uri)
                    }

                    imageId = 0
                    loadImage(imageId)
                }
            }
            else -> {
                Log.e(TAG, "Unknown request code: $requestCode")
                return
            }
        }
    }

    fun loadImage(id: Int) {
        val imageStream = getContentResolver().openInputStream(images[id])
        val yourSelectedImage = BitmapFactory.decodeStream(imageStream)
        var mutableBitmap = yourSelectedImage.copy(Bitmap.Config.ARGB_8888, true);

        // TODO Scale image to view
        val scaledBitmap = getResizedBitmap(mutableBitmap, drawingView.canvasWidth, drawingView.canvasHeight)
        drawingView.setDrawing(scaledBitmap)
        drawingView.invalidate()
    }

    // TODO Move to interactor later
    fun getResizedBitmap(bm:Bitmap, newWidth:Int, newHeight:Int) : Bitmap {
        val width = bm.getWidth()
        val height = bm.getHeight()
        val scaleWidth = newWidth.toFloat() / width
        val scaleHeight = newHeight.toFloat() / height
        // CREATE A MATRIX FOR THE MANIPULATION
        val matrix = Matrix()
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        val resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false)
        bm.recycle()
        return resizedBitmap
    }

    fun hideSystemUI() {
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
        val decorView = window.getDecorView()
        decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                or View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                or View.SYSTEM_UI_FLAG_IMMERSIVE)
    }
}