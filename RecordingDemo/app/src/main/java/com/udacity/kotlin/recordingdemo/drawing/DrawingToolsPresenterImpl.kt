package com.udacity.kotlin.recordingdemo.drawing

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.view.View
import android.widget.Button
import android.widget.SeekBar
import android.widget.TextView
import com.nononsenseapps.filepicker.FilePickerActivity
import com.pes.androidmaterialcolorpickerdialog.ColorPicker
import com.udacity.kotlin.recordingdemo.R

/**
 * Created by james on 2/26/16.
 */
class DrawingToolsPresenterImpl(activity: DrawingActivity) : DrawingToolsPresenter, View.OnClickListener, View.OnLongClickListener,
        SeekBar.OnSeekBarChangeListener {
    var activity : DrawingActivity

    // TODO remove later
    lateinit var btnColorOne : Button
    lateinit var btnColorTwo : Button
    lateinit var btnColorThree : Button
    lateinit var btnColorFour : Button
    lateinit var btnColorFive : Button
    lateinit var btnColorWhite : Button
    lateinit var btnColorBlack : Button
    lateinit var btnClear : Button
    lateinit var btnLoad : Button
    lateinit var btnNext : Button
    lateinit var btnPrevious : Button

    lateinit var seekbar : SeekBar

    lateinit var penThicknessLabel : TextView

    // Default palette
    var colorOne = Color.CYAN
    var colorTwo = Color.BLUE
    var colorThree = Color.YELLOW
    var colorFour = Color.GRAY
    var colorFive = Color.DKGRAY
    val colorWhite = Color.WHITE
    val colorBlack = Color.BLACK

    lateinit var colorPickerDialog : AlertDialog
    var selectedPickerButton : Button? = null

    init {
        this.activity = activity
        initializeTools()
    }

    fun initializeTools() {
        // TODO Fix later
        with(activity) {
            btnColorOne = findViewById(R.id.btn_color_one) as Button
            btnColorTwo = findViewById(R.id.btn_color_two) as Button
            btnColorThree = findViewById(R.id.btn_color_three) as Button
            btnColorFour = findViewById(R.id.btn_color_four) as Button
            btnColorFive = findViewById(R.id.btn_color_five) as Button
            btnColorWhite = findViewById(R.id.btn_color_white) as Button
            btnColorBlack = findViewById(R.id.btn_color_black) as Button
            btnClear = findViewById(R.id.btn_clear) as Button
            btnLoad = findViewById(R.id.btn_load) as Button
            btnNext = findViewById(R.id.btn_next) as Button
            btnPrevious = findViewById(R.id.btn_previous) as Button
        }

        btnColorOne.setTag(colorOne)
        btnColorTwo.setTag(colorTwo)
        btnColorThree.setTag(colorThree)
        btnColorFour.setTag(colorFour)
        btnColorFive.setTag(colorFive)
        btnColorWhite.setTag(colorWhite)
        btnColorBlack.setTag(colorBlack)


        btnColorOne.background.setColorFilter(colorOne, PorterDuff.Mode.MULTIPLY)
        btnColorTwo.background.setColorFilter(colorTwo, PorterDuff.Mode.MULTIPLY)
        btnColorThree.background.setColorFilter(colorThree, PorterDuff.Mode.MULTIPLY)
        btnColorFour.background.setColorFilter(colorFour, PorterDuff.Mode.MULTIPLY)
        btnColorFive.background.setColorFilter(colorFive, PorterDuff.Mode.MULTIPLY)
        btnColorWhite.background.setColorFilter(colorWhite, PorterDuff.Mode.MULTIPLY)
        btnColorBlack.background.setColorFilter(colorBlack, PorterDuff.Mode.MULTIPLY)

        val buttons = arrayOf(btnColorOne, btnColorTwo, btnColorThree, btnColorFour, btnColorFive,
                btnColorWhite, btnColorBlack)

        for ( button in buttons) {
            button.setOnClickListener(this)
            button.setOnLongClickListener(this)
        }

        seekbar = activity.findViewById(R.id.seekbar) as SeekBar
        seekbar.setOnSeekBarChangeListener(this)

        penThicknessLabel = activity.findViewById(R.id.nav_pen_thickness) as TextView
        penThicknessLabel.text = "Pen Size: 5"

        btnClear.setOnClickListener {
            clearDrawing()
        }

        btnLoad.setOnClickListener { loadDrawing() }

        btnNext.setOnClickListener { loadNextImage()}
        btnPrevious.setOnClickListener { loadPreviousImage() }
    }

    override fun changePaletteColor(button: Button) {
        throw UnsupportedOperationException()
    }

    override fun changePenSize() {
        throw UnsupportedOperationException()
    }

    override fun clearDrawing() {
        activity.drawingView.resetDrawing()
    }

    override fun loadDrawing() {
        val intent = Intent(activity as Context, FilePickerActivity::class.java)
        intent.putExtra(FilePickerActivity.EXTRA_START_PATH, android.os.Environment.getExternalStoragePublicDirectory(
                android.os.Environment.DIRECTORY_MOVIES +"/Poe/").getPath())
        intent.putExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, true)
        activity.startActivityForResult(intent, activity.FILE_CODE)
    }

    override fun loadPalette() {
        throw UnsupportedOperationException()
    }

    fun loadPreviousImage() {
        if (activity.images.size == 0)
            return

        activity.imageId--
        if (activity.imageId < 0)
            activity.imageId = 0
        activity.loadImage(activity.imageId)
    }

    override fun loadNextImage() {
        if (activity.images.size == 0)
            return

        activity.imageId++
        if (activity.imageId > activity.images.size - 1) {
            activity.imageId = activity.images.size - 1
        }

        activity.loadImage(activity.imageId)
    }

    override fun savePalette() {
        throw UnsupportedOperationException()
    }

    override fun undoDrawing() {
        throw UnsupportedOperationException()
    }

    override fun onClick(view: View) {
        val button = view as Button
        val color = button.getTag() as Int
        activity.drawingView.setDrawingColor(color)
    }

    override fun onLongClick(view: View?): Boolean {
        val button = view as Button
        val color = button.tag as Int

        val cp = ColorPicker(activity, Color.red(color), Color.green(color), Color.blue(color))
        cp.show()
        val okColor = cp.findViewById(R.id.okColorButton) as Button
        okColor.setOnClickListener(object: View.OnClickListener {
            override fun onClick(v: View) {
                /* Or the android RGB Color (see the android Color class reference) */
                val selectedColorRGB = cp.getColor()
                button.setTag(selectedColorRGB)
                button.background.setColorFilter(selectedColorRGB, PorterDuff.Mode.MULTIPLY)
                cp.dismiss()
            }
        })
        return true
    }

    override fun onProgressChanged(sb: SeekBar?, progress: Int, fromUser: Boolean) {
        activity.drawingView.setDrawingThickness(progress.toFloat())
        penThicknessLabel.text = "Pen Size: "+progress

    }

    override fun onStartTrackingTouch(p0: SeekBar?) {

    }

    override fun onStopTrackingTouch(p0: SeekBar?) {

    }
}