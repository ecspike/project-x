package com.udacity.kotlin.recordingdemo.drawing

/**
 * Created by james on 2/19/16.
 */
interface DrawingInteractor {
    fun saveImage()
    fun loadImage()


    // Not in first cut
    /*
        fun setColor(p:Paint)
        fun setToolSize(i:Int)
        fun loadPalette()
        fun savePalette
     */
}