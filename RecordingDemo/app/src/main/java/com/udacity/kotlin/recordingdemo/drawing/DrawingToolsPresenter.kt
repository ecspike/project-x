package com.udacity.kotlin.recordingdemo.drawing

import android.widget.Button

/**
 * Created by james on 2/19/16.
 */
interface DrawingToolsPresenter {
    fun changePenSize()

    fun changePaletteColor(button : Button)

    fun loadDrawing()
    fun undoDrawing()
    fun clearDrawing()
    fun loadNextImage()
    fun loadPalette()
    fun savePalette()
}