package com.udacity.kotlin.recordingdemo.recording

import android.app.Application
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.hardware.display.DisplayManager
import android.hardware.display.VirtualDisplay
import android.media.CamcorderProfile
import android.media.MediaRecorder
import android.media.projection.MediaProjection
import android.media.projection.MediaProjectionManager
import android.os.IBinder
import android.util.DisplayMetrics
import android.util.Log
import org.jetbrains.anko.windowManager
import java.io.IOException

class RecordingInteractorImpl(context: Context): RecordingInteractor {
    companion object {
        val TAG = RecordingInteractorImpl::class.java.toString()
    }

    var screenDensity: Int = 0

    lateinit var virtualDisplay: VirtualDisplay


    var DISPLAY_WIDTH = 720
    var DISPLAY_HEIGHT = 1280

    var mediaRecorder: MediaRecorder? = null
    var mediaProjection: MediaProjection? = null
    var mediaProjectionManager: MediaProjectionManager? = null
    lateinit var mediaProjectionCallback: MediaProjectionCallback

    lateinit var currentSession : RecordingSession
    lateinit var context: Context

    var recordingStatus = false

    init {
        var metrics = DisplayMetrics()
        this.context = context
        context.windowManager.defaultDisplay.getMetrics(metrics)
        screenDensity = metrics.densityDpi

        mediaRecorder = MediaRecorder()

        mediaProjectionManager = context.getSystemService(Context.MEDIA_PROJECTION_SERVICE) as MediaProjectionManager

        mediaProjectionCallback = MediaProjectionCallback()

        this.context = context
    }

    override fun initRecorder() {
        with (mediaRecorder!!) {
            reset()
            setAudioSource(MediaRecorder.AudioSource.MIC)
            setVideoSource(MediaRecorder.VideoSource.SURFACE)

            var camcorderProfile = CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH)
            val file = java.io.File(android.os.Environment.getExternalStoragePublicDirectory(
                    android.os.Environment.DIRECTORY_MOVIES).absolutePath);
            //file.mkdirs()

            val filename = file.absolutePath +"/"+ java.util.Date().time + ".mp4"

            currentSession = RecordingSession(camcorderProfile, filename)

            setProfile(camcorderProfile)

            // Set to profile size which may be bigger than 720p defaults
            // TODO Write function to capture full frame if it isn't 16:9 or 16:10
            DISPLAY_WIDTH = 2048
            DISPLAY_HEIGHT = 1500
            //DISPLAY_WIDTH = currentSession.displayWidth
            //DISPLAY_HEIGHT = currentSession.displayHeight
            setVideoSize(DISPLAY_WIDTH, DISPLAY_HEIGHT)
            // make this settable
            // Get the directory for the user's public pictures directory.


            setOutputFile(currentSession.filename)
        }
    }

    override fun setupRecording(resultCode: Int, data: Intent?) {
        initRecorder()
        prepareRecorder()
        mediaProjection = mediaProjectionManager?.getMediaProjection(resultCode, data)
        mediaProjection?.registerCallback(mediaProjectionCallback, null)
        virtualDisplay = createVirtualDisplay()
        mediaRecorder?.start()
    }

    // Override Runnable anon class for kotlin
    fun runnable(f: () -> Unit):Runnable = object : Runnable { override fun run(){f()}}

    override fun shareScreen() {
            virtualDisplay = createVirtualDisplay()
            mediaRecorder?.start()
    }

    override fun stopScreenSharing() {
        try {
            mediaRecorder?.stop()
            mediaRecorder?.reset()
            virtualDisplay.release()

            // send broadcast
            val intent = Intent()
            intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES)
            intent.setAction("com.udacity.kotlin.RecordingMessage")
            intent.putExtra("filename", currentSession.filename)
            context.sendBroadcast(intent)
        } catch( ex: IllegalStateException) {
            // onStop called when mediaRecorder is already stopped
            // e.g. orientationChange

        }
    }

    override fun createVirtualDisplay(): VirtualDisplay {
        return mediaProjection!!.createVirtualDisplay("MainActivity",
                DISPLAY_WIDTH, DISPLAY_HEIGHT, screenDensity,
                DisplayManager.VIRTUAL_DISPLAY_FLAG_PRESENTATION,
                mediaRecorder!!.getSurface(), null, null)
    }

    // TODO: Move to Interactor
    override fun prepareRecorder() {
        try {
            mediaRecorder?.prepare()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    override fun onDestroy() {
        if (mediaProjection != null) {
            mediaProjection?.stop()
            mediaProjection = null
        }
    }

    inner class MediaProjectionCallback : MediaProjection.Callback() {
        override fun onStop() {
            try {
                mediaRecorder!!.stop()
                mediaRecorder!!.reset()
                Log.v(TAG, "Recording Stopped")
                initRecorder()
                prepareRecorder()
            } catch( ex: IllegalStateException) {
                // onStop called when mediaRecorder is already stopped
                // e.g. orientationChange

            }
        }
        }
}
