package com.udacity.kotlin.recordingdemo.main

import android.app.Activity
import android.content.Intent

interface  MainPresenter {
    fun onResume()
    fun onRecordingStart()
    fun onRecordingStop()
    fun onDestroy()
    fun verifyDevicePermissions(activity: Activity) : Int
    fun setupRecording(resultCode:Int, data: Intent?)
}