package com.udacity.kotlin.recordingdemo.main

import android.app.Activity
import android.view.View

interface MainView {
    fun toggleRecording(view: View)
    fun hideSystemUI()
    fun showSystemUI()
    fun getActivity(): Activity
    fun showChangeNameDialog(filename: String)
}
