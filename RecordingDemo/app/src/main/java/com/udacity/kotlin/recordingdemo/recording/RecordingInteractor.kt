package com.udacity.kotlin.recordingdemo.recording

import android.content.Intent
import android.hardware.display.VirtualDisplay

interface RecordingInteractor {
    fun shareScreen()
    fun stopScreenSharing()
    fun createVirtualDisplay(): VirtualDisplay
    fun prepareRecorder()
    fun initRecorder()

    fun onDestroy()
    fun setupRecording(resultCode: Int, data: Intent?)
}
