package com.udacity.kotlin.recordingdemo.main

import android.app.Activity
import android.content.ClipData
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.app.AppCompatActivity.RESULT_OK
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.ToggleButton
import com.nononsenseapps.filepicker.FilePickerActivity
import com.udacity.kotlin.recordingdemo.drawing.DrawingActivity
import com.udacity.kotlin.recordingdemo.R
import org.jetbrains.anko.find
import org.jetbrains.anko.onClick
import org.jetbrains.anko.toast
import java.io.File
import java.util.*

class MainActivity : Activity(), MainView, NavigationView.OnNavigationItemSelectedListener {
    val TAG = MainActivity::class.java.toString()

    companion object {
        lateinit var mainPresenter: MainPresenter
    }

    val PERMISSION_CODE = 1
    val FILE_CODE = 2



    val statusView by lazy {find<TextView>(R.id.statusView)}
    val toolbar by lazy {find<Toolbar>(R.id.toolbar)}

    // widgets
    lateinit private var toggleButton: ToggleButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //val toolbar = findViewById(R.id.toolbar) as Toolbar
        //setSupportActionBar(toolbar)

        mainPresenter = MainPresenterImpl(this)

        mainPresenter.verifyDevicePermissions(this@MainActivity)

        toggleButton = find<ToggleButton>(R.id.toggle)
        toggleButton.onClick { v -> toggleRecording(v!!) }


        statusView.text = ""

        val drawer = findViewById(R.id.drawer_layout) as DrawerLayout
        val toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer.setDrawerListener(toggle)
        toggle.syncState()

        val navigationView = findViewById(R.id.navView) as NavigationView
        navigationView.setNavigationItemSelectedListener(this)

        val toolbar = findViewById(R.id.toolbar) as Toolbar
        if (toolbar != null) {
           toolbar.title = getString(R.string.app_name)
        }

        hideSystemUI()
    }

    override fun onStop() {
        super.onStop()
        onDestroy()
        hideTouches()
    }

    override fun onDestroy() {
        super.onDestroy()
        hideTouches()
        // TODO: Destroy Recorder Interactor Media Projection
        //mainPresenter.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            PERMISSION_CODE -> {
                if (resultCode != RESULT_OK) {
                    toast("Screen Cast Permission Denied")
                    toggleButton.isChecked = false
                    return
                }

                showTouches()
                //hideControls()
                mainPresenter.setupRecording(resultCode, data)
                statusView.text = statusView.text.toString() + "${getDate()} Recording started.\n"
            }
            FILE_CODE -> {
                if (resultCode != RESULT_OK) {
                    toast("File Share Failed")
                    return
                }

                val clip = data?.getClipData()

                if (clip != null) {
                    var paths = ArrayList<Uri>()
                    for (index in 0..clip.itemCount-1) {
                        paths.add(clip.getItemAt(index).uri)
                    }
                    val shareIntent = Intent()
                    with(shareIntent) {
                        setAction(Intent.ACTION_SEND_MULTIPLE)
                        putParcelableArrayListExtra(Intent.EXTRA_STREAM, paths)
                        setType("video/*")
                    }
                    startActivity(Intent.createChooser(shareIntent, "Share videos to ..."))
                }
            }
            else -> {
                Log.e(TAG, "Unknown request code: $requestCode")
                return
            }
        }
    }

    fun hideTouches() {
        // Hide user touches
        Settings.System.putInt(this.contentResolver, "show_touches", 0)
    }

    fun showTouches() {
        // Show user touches with overlay circle
        Settings.System.putInt(this.contentResolver, "show_touches", 1)
    }

    fun getDate() : CharSequence {
        return android.text.format.DateFormat.format("yyyy-MM-dd hh:mm:ss", Date())
    }


    override fun toggleRecording(view: View) {
        if ((view as ToggleButton).isChecked) {
            mainPresenter.onRecordingStart()
        } else {
            statusView.text = statusView.text.toString() + "${getDate()} Recording stopped.\n"
            Log.v(TAG, "Recording stopped.")
            //hideTouches()
            mainPresenter.onRecordingStop()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    fun changeFilename(from:String, to:String) {
        var file = File(from)
        var dest = File(to)
        file.renameTo(dest)
    }

    override fun showChangeNameDialog(filename:String) {
        var builder : AlertDialog.Builder = AlertDialog.Builder(getActivity());

        val textEntry = EditText(this)
        textEntry.setText(filename, TextView.BufferType.EDITABLE)

        builder.setTitle(R.string.change_filename).setView(textEntry)


        builder.setPositiveButton(R.string.ok, object: DialogInterface.OnClickListener {
            override fun onClick(dialog : DialogInterface, id : Int) {
                // User clicked OK button
                val name = textEntry.getText().toString()
                // change filename
                changeFilename(filename, name)
                statusView.text = statusView.text.toString() +
                        "${getDate()} Filename changed to ${name}.\n"
            }
        })
        builder.setNegativeButton(R.string.cancel, object: DialogInterface.OnClickListener {
            override fun onClick(dialog : DialogInterface, id : Int) {
                // User clicked Cancel button
            }
        })

        // 3. Get the AlertDialog from create()
        var dialog : AlertDialog = builder.create()
        dialog.show()
    }

    override fun hideSystemUI() {
       // Set the IMMERSIVE flag.
       // Set the content to appear under the system bars so that the content
       // doesn't resize when the system bars hide and show.
       val decorView = window.getDecorView()
       decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
               or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
               or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
               or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
               or View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
               or View.SYSTEM_UI_FLAG_IMMERSIVE)
   }

    // This snippet shows the system bars. It does this by removing all the flags
    // except for the ones that make the content appear under the system bars.
    override fun showSystemUI() {
        val decorView = window.getDecorView()
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
    }

    override fun getActivity(): Activity {
        return this
    }

    override fun onResume() {
        super.onResume()
        mainPresenter.onResume()
    }

    override fun onPause() {
        super.onPause()
        mainPresenter.onDestroy()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val id = item.itemId

        if (id == R.id.nav_camera) {
            // Handle the camera action
            val intent = Intent(this as Context, DrawingActivity::class.java)
            startActivity(intent)
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {
            //Snackbar.make(find(android.R.id.content),"Share Stuff", Snackbar.LENGTH_LONG).show()
            // Start File Picker intent
            val intent = Intent(this as Context, FilePickerActivity::class.java)
            intent.putExtra(FilePickerActivity.EXTRA_START_PATH, android.os.Environment.getExternalStoragePublicDirectory(
                    android.os.Environment.DIRECTORY_MOVIES +"/Poe/").getPath())
            intent.putExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, true)
            startActivityForResult(intent, FILE_CODE)
        } else if (id == R.id.nav_send) {

        }

        val drawer = findViewById(R.id.drawer_layout) as DrawerLayout
        drawer.closeDrawer(GravityCompat.START)
        return false
    }
}
